import dotenv from 'dotenv';
import Koa from 'koa';
import Pug from 'koa-pug';
import Router from '@koa/router';
import kStatic from 'koa-static';
import { dirname, join } from 'path';
import pg from 'pg';
import { fileURLToPath } from 'url';

const __filename = fileURLToPath(import.meta.url);
const __dirname = dirname(__filename);

dotenv.config();

const pool = new pg.Pool({ connectionString: process.env.DATABASE_URL });

const colors = [];
for (let i = 0; i < 256; i += 1) {
  const red = "#ff0000" + (i > 9 ? i.toString(16) : `0${i}`);
  const blue = "#0000ff" + (i > 9 ? i.toString(16) : `0${i}`);

  colors.push({ className: `r${i}`, color: red });
  colors.push({ className: `b${i}`, color: blue });
}

const app = new Koa();
app.use(kStatic(join(__dirname, './public')));
new Pug({
  viewPath: join(__dirname, './views'),
  app
});

const router = new Router({ prefix: '/api' });
router.get('/rates', async ctx => {
  const result = await pool.query(`
    SELECT name as county, total_cases::float / population as rate
    FROM covid_reports
    JOIN counties ON (county_id = id)
    WHERE date_id IN (SELECT MAX(id) from dates)
    ORDER BY name
  `);
  ctx.body = {
    data: result.rows,
  };
});
router.get('/accel', async ctx => {
  const result = await pool.query(`
    SELECT c.name as county, today.cases - coalesce(yesterday.cases, 0) as rate
    FROM (SELECT county_id, cases_reported as cases FROM covid_reports WHERE date_id IN (SELECT MAX(id) FROM dates)) today
    JOIN (SELECT county_id, cases_reported as cases FROM covid_reports WHERE date_id IN (SELECT id FROM dates ORDER BY id DESC OFFSET 1 LIMIT 1)) yesterday ON (today.county_id = yesterday.county_id)
    JOIN counties c ON (yesterday.county_id = c.id)
    ORDER BY c.name
  `);
  ctx.body = {
    data: result.rows,
  };
});

app
  .use(router.routes())
  .use(router.allowedMethods())
  .use(async ctx => await ctx.render('dashboard', { colors }));

app.listen(process.env.PORT);
