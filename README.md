# Data Visualization Project

This is my submission for the discussion for Week Three of COSC 6351.

I decided to go beyond the requirements of the project and put into practice
the information that I learned from both chapters three and four of the
textbook.

## Problem

The project description reads:

> Download an information visualization tool, such as tableau, QlikView or
> Spotfire, or use its online version directly. Use your own data set or
> demonstration data sets comes with the tool. Try to analyze the data to solve
> a specific problem and share your problem as well as findings with the class.
> You may screen capture the data visualization results and upload the result
> picture with your post.

My dashboard is designed to help workers at the Texas Department of State
Health Services. It shows:

* Acceleration of recent identified cases
* Normalized cumulative cases of infection as a percentage of populace

## Solution

The following is a summary of the components that I've chosen for my project.

* **Visualization**: I have chosen to use D3, a ECMAscript library for
  manipulating documents based on data.
* **Data warehouse architecture**: I am using the centralized data warehouse
  architecture with the following identified components:
  * _Source system_: These are publicly available Excel spreadsheets provided
    by the Texas Department of State Health Services.
  * _Staging area_: The ETL scripts are a set of Node.js scripts used to
    **extract** (fetch) the data from the source system, **transform** it by
    reading it from the Microsoft Excel file format, and loading it into a
    PostgreSQL database.
  * _Normalized relational warehouse_: This is a PostgreSQL database instance
    that contains a snowflake schema to maintain the data for the
    visualization.
  * _End user access_: This is an HTML5 Web-based application created to show
    the different visualizations.

### The source data

The source data comes from these Internet-accessible resources:

* **Cases over Time by County**:
  https://dshs.texas.gov/coronavirus/TexasCOVID19DailyCountyCaseCountData.xlsx
* **Fatalities over Time by County**:
  https://dshs.texas.gov/coronavirus/TexasCOVID19DailyCountyFatalityCountData.xlsx
* **SVG of Texas Counties**:
  https://upload.wikimedia.org/wikipedia/commons/9/9c/Map_of_Texas_highlighting_Mason_County.svg
* **Center for Health Statistics Public Health Regions**:
  https://dshs.texas.gov/chs/info/info_txco.shtm

### The ETL scripts

You can find the script for the ETL in the `./etl` directory of this project.
The script fetches all COVID-related data and upserts it into the data
warehouse.

### The data warehouse

The data warehouse contains a snowflake schema with the following structure.

* **Fact table**: Case and fatality information by county by day
* **Dimension tables**:
  * **Date dimension**: Per _The Data Warehouse Toolkit_ by Ralph Kimball, this
    table contains the each date for the inserted data along with many pieces
    of metadata about the date such as the day name, day of the week, month,
    quarter, and whether or not its a weekend.
  * **County dimension**: Contains the counties in the state of Texas and a
    reference to the region that the county exists in.
  * **Public health region dimension** Contains the public health region.

### The end user access

The end user interface is powered by a Koa.js server that serves the HTML5,
CSS, and client-side ECMAscript. The user interface makes asynchronous HTTP
calls the server to get the data to present. That data is then rendered using
ECMAscript calls to the D3 library.

All content for the end user access is in the `app` directory.

### The tools

I am learning a new text editor named Kakoune. I used it in a `tmux` session to
generate the content for all files in this project.

## Try it out

To run this locally, make sure you have the following installed (and running)
on your computer.

* Node.js version 14 or higher
* Postgresql 11

Clone this repository. Create a `.env` file in the root directory with a single
entry `DATABASE_URL` and set it to an appropriate connection string. Run the
following commands.

```shell
npm install
npm run etl
npm start
```

Then, open your browser to http://localhost:8082 to see the dashboard.
