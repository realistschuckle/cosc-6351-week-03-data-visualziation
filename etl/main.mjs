import dotenv from 'dotenv';
import fetch from 'node-fetch';
import fs from 'fs';
import { dirname, join } from 'path';
import pg from 'pg';
import unzipper from 'unzipper';
import { fileURLToPath } from 'url';
import xml2js from 'xml2js';

const __filename = fileURLToPath(import.meta.url);
const __dirname = dirname(__filename);
const { Parser } = xml2js;
const { readFile } = fs.promises;
const asyncWrapper = fn => () => fn().then(x => {}, e => {console.error(e); throw e;});

dotenv.config();
const pool = new pg.Pool({ connectionString: process.env.DATABASE_URL });
const data = { headers: null, cases: null, fatalities: null };

async function parseSheet(sheet, name, prefix) {
  if (!sheet.ok || sheet.status >= 400) {
    await logFetchToDatabase(name, sheet);
    data.fatalities = new Error('Failed to fetch data');
  } else {
    const fatalitiesPath = join(__dirname, 'data', name);
    sheet.body.pipe(unzipper.Extract({ path: fatalitiesPath }));
    sheet.body.on('end', asyncWrapper(async () => {
      const sheetDataPath = join(fatalitiesPath, 'xl', 'worksheets', 'sheet1.xml');
      const sheetStringPath = join(fatalitiesPath, 'xl', 'sharedStrings.xml');
      const sheetDataContent = await readFile(sheetDataPath);
      const stringsContent = await readFile(sheetStringPath);
      const parser = new Parser(sheetDataContent);
      const sheetStrings = await parser.parseStringPromise(stringsContent);
      const { worksheet: { sheetData } } = await parser.parseStringPromise(sheetDataContent);
      const strings = sheetStrings.sst.si
        .map(x => x.t[0])
        .map(x => {
          if (x && x.startsWith && x.startsWith(prefix)) {
            const entry = x.substring(prefix.length).replace(/_x000D_/g, '').trim();
            const d = new Date(entry);
            d.setFullYear(2020);
            return d;
          } else if (x && x.replace) {
            return x.replace(/_x000D_/g, '');
          } else {
            return x;
          }
        });
      data.headers = sheetData[0].row[2].c.map(x => parseCell(x, strings));
      data[name] = sheetData[0].row.slice(3, 256).map(row => row.c.map(x => parseCell(x, strings)));

      if (data.cases && data.fatalities) {
        await upsertData();
      }
    }));
  }
}

async function upsertData() {
  const client = await pool.connect();
  try {
    await client.query('BEGIN');

    const dates = data.headers.slice(2);
    await client.query(`
      INSERT INTO dates
      SELECT TO_CHAR(datum,'yyyymmdd')::INT AS id,
        datum AS date_actual,
        EXTRACT(epoch FROM datum) AS epoch,
        TO_CHAR(datum,'fmDDth') AS day_suffix,
        TO_CHAR(datum,'Day') AS day_name,
        EXTRACT(isodow FROM datum) AS day_of_week,
        EXTRACT(DAY FROM datum) AS day_of_month,
        datum - DATE_TRUNC('quarter',datum)::DATE +1 AS day_of_quarter,
        EXTRACT(doy FROM datum) AS day_of_year,
        TO_CHAR(datum,'W')::INT AS week_of_month,
        EXTRACT(week FROM datum) AS week_of_year,
        TO_CHAR(datum,'YYYY"-W"IW-') || EXTRACT(isodow FROM datum) AS week_of_year_iso,
        EXTRACT(MONTH FROM datum) AS month_actual,
        TO_CHAR(datum,'Month') AS month_name,
        TO_CHAR(datum,'Mon') AS month_name_abbreviated,
        EXTRACT(quarter FROM datum) AS quarter_actual,
        CASE
          WHEN EXTRACT(quarter FROM datum) = 1 THEN 'First'
          WHEN EXTRACT(quarter FROM datum) = 2 THEN 'Second'
          WHEN EXTRACT(quarter FROM datum) = 3 THEN 'Third'
          WHEN EXTRACT(quarter FROM datum) = 4 THEN 'Fourth'
        END AS quarter_name,
        EXTRACT(isoyear FROM datum) AS year_actual,
        datum +(1 -EXTRACT(isodow FROM datum))::INT AS first_day_of_week,
        datum +(7 -EXTRACT(isodow FROM datum))::INT AS last_day_of_week,
        datum +(1 -EXTRACT(DAY FROM datum))::INT AS first_day_of_month,
        (DATE_TRUNC('MONTH',datum) +INTERVAL '1 MONTH - 1 day')::DATE AS last_day_of_month,
        DATE_TRUNC('quarter',datum)::DATE AS first_day_of_quarter,
        (DATE_TRUNC('quarter',datum) +INTERVAL '3 MONTH - 1 day')::DATE AS last_day_of_quarter,
        TO_DATE(EXTRACT(isoyear FROM datum) || '-01-01','YYYY-MM-DD') AS first_day_of_year,
        TO_DATE(EXTRACT(isoyear FROM datum) || '-12-31','YYYY-MM-DD') AS last_day_of_year,
        TO_CHAR(datum,'mmyyyy') AS mmyyyy,
        TO_CHAR(datum,'mmddyyyy') AS mmddyyyy,
        CASE
          WHEN EXTRACT(isodow FROM datum) IN (6,7) THEN TRUE
          ELSE FALSE
        END AS weekend_indr
      FROM (SELECT $1::DATE+ SEQUENCE.DAY AS datum
            FROM GENERATE_SERIES (0,$2) AS SEQUENCE (DAY)
            GROUP BY SEQUENCE.DAY) DQ
      ORDER BY 1;
    `, [dates[0], dates.length])

    for (let countyIndex = 0; countyIndex < data.fatalities.length; countyIndex += 1) {
      const [name, population] = data.fatalities[countyIndex];
      await client.query(`
        INSERT INTO counties (id, name, population)
        VALUES ($1, $2, $3);
      `, [countyIndex, name, Number.parseInt(population)]);
    }

    for (let countyId = 0; countyId < data.fatalities.length; countyId += 1) {
      const countyCaseRecord = data.cases[countyId];
      const countyFatalityRecord = data.fatalities[countyId];
      let lastFatalityReport = 0;
      let lastCaseReport = 0;
      for (let dateIndex = 2; dateIndex < countyFatalityRecord.length; dateIndex += 1) {
        const date = data.headers[dateIndex];
        if (!date) {
          break;
        }
        const dateId = date.getFullYear() * 10000 + (date.getMonth() + 1) * 100 + date.getDate();
        const caseReport = Number.parseInt(countyCaseRecord[dateIndex]);
        const fatalityReport = Number.parseInt(countyFatalityRecord[dateIndex]);
        const dailyCaseReport = caseReport - lastCaseReport;
        const dailyFatalityReport = fatalityReport - lastFatalityReport;

        await client.query(`
          INSERT INTO covid_reports (county_id, date_id, fatalities_reported, cases_reported, total_cases, total_fatalities)
          VALUES ($1, $2, $3, $4, $5, $6);
        `, [countyId, dateId, dailyFatalityReport, dailyCaseReport, caseReport, fatalityReport]);
        lastFatalityReport = fatalityReport;
        lastCaseReport = caseReport;
      }
    }

    await client.query('COMMIT');
  } catch (e) {
    await client.query('ROLLBACK');
    throw e;
  } finally {
    client.release();
    pool.end();
  }
}

async function logFetchToDatabase(name, result) {
  await logToDatabase(name, result.status, result.statusText);
}

async function logToDatabase(name, status, statusText) {
  console.error(name, status, statusText);
  await pool.query(`
    INSERT INTO imports (message, status, statusText)
    VALUES ($1, CURRENT_TIMESTAMP)
  `, [name, status, statusText]);
}

function parseCell(cell, strings) {
  return cell['$'].t === 's'
       ? strings[Number.parseInt(cell.v[0])]
       : Number.parseFloat(cell.v[0]);
}

(async () => {

const client = await pool.connect();
try {
  await client.query('BEGIN');

  await client.query(`DROP TABLE IF EXISTS covid_reports`);
  await client.query(`DROP TABLE IF EXISTS counties`);
  await client.query(`DROP TABLE IF EXISTS regions`);
  await client.query(`DROP TABLE IF EXISTS dates`);

  await client.query(`
    CREATE TABLE IF NOT EXISTS imports (
      id SERIAL PRIMARY KEY,
      created TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
      message VARCHAR(100) NOT NULL,
      status INT,
      statusText TEXT
    )
  `);

  await client.query(`
    CREATE TABLE dates (
      id                       SERIAL PRIMARY KEY,
      date_actual              DATE NOT NULL,
      epoch                    BIGINT NOT NULL,
      day_suffix               VARCHAR(4) NOT NULL,
      day_name                 VARCHAR(9) NOT NULL,
      day_of_week              INT NOT NULL,
      day_of_month             INT NOT NULL,
      day_of_quarter           INT NOT NULL,
      day_of_year              INT NOT NULL,
      week_of_month            INT NOT NULL,
      week_of_year             INT NOT NULL,
      week_of_year_iso         CHAR(10) NOT NULL,
      month_actual             INT NOT NULL,
      month_name               VARCHAR(9) NOT NULL,
      month_name_abbreviated   CHAR(3) NOT NULL,
      quarter_actual           INT NOT NULL,
      quarter_name             VARCHAR(9) NOT NULL,
      year_actual              INT NOT NULL,
      first_day_of_week        DATE NOT NULL,
      last_day_of_week         DATE NOT NULL,
      first_day_of_month       DATE NOT NULL,
      last_day_of_month        DATE NOT NULL,
      first_day_of_quarter     DATE NOT NULL,
      last_day_of_quarter      DATE NOT NULL,
      first_day_of_year        DATE NOT NULL,
      last_day_of_year         DATE NOT NULL,
      mmyyyy                   CHAR(6) NOT NULL,
      mmddyyyy                 CHAR(10) NOT NULL,
      weekend_indr             BOOLEAN NOT NULL
    );
  `);

  await client.query(`
    CREATE TABLE counties (
      id INTEGER PRIMARY KEY,
      name VARCHAR(30) NOT NULL,
      population INTEGER NOT NULL
    );
  `);

  await client.query(`
    CREATE TABLE covid_reports (
      county_id INTEGER NOT NULL,
      date_id INTEGER NOT NULL,
      fatalities_reported INTEGER NOT NULL,
      cases_reported INTEGER NOT NULL,
      total_cases INTEGER NOT NULL,
      total_fatalities INTEGER NOT NULL,
      PRIMARY KEY (county_id, date_id)
    );
  `);

  await client.query('COMMIT');
} catch (e) {
  await client.query('ROLLBACK');
  throw e;
} finally {
  client.release();
}

const fetches = Promise.all([
  fetch('https://dshs.texas.gov/coronavirus/TexasCOVID19DailyCountyCaseCountData.xlsx'),
  fetch('https://dshs.texas.gov/coronavirus/TexasCOVID19DailyCountyFatalityCountData.xlsx'),
]);

const [cases, fatalities] = await fetches;
await parseSheet(cases, 'cases', 'Cases');
await parseSheet(fatalities, 'fatalities', 'Fatalities');

})().then(() => {}, e => { console.error(e); });
